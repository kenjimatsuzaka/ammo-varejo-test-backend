process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../dist/app').default;


const should = chai.should();

chai.use(chaiHttp);

describe('Products', () => {
  describe('/GET product', () => {
    it('passing no query string, it should get all products', (done) => {
      chai.request(app)
        .get('/products')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.count.should.be.eql(41);
          done();
        });
    });
    it('passing query string (with 0 occurrences), it should get no products', (done) => {
      chai.request(app)
        .get('/products?q=foo')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.count.should.be.eql(0);
          done();
        });
    });
    it('passing wrong query string, it should get all products', (done) => {
      chai.request(app)
        .get('/products?meWrong=lencol')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.count.should.be.eql(41);
          done();
        });
    });
    it('passing query string (with occurrences), it should get the expected products', (done) => {
      chai.request(app)
        .get('/products?q=cama')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.count.should.be.eql(1);
          done();
        });
    });
  });
});
