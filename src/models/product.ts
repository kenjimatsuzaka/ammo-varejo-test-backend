import * as Sequelize from 'sequelize';
import { SequelizeAttributes } from 'typings/SequelizeAttributes'

export interface ProductAttributes {
  id?: number;
  name: string;
  description: string;
  productType: string;
  imageSrc1: string;
  imageSrc2: string;
  imageSrc3: string;
  imageSrc4: string;
  price: number;
  discount: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
};

export interface ProductInstance extends Sequelize.Instance<ProductAttributes>, ProductAttributes {
};

export const ProductFactory = (sequelize: Sequelize.Sequelize, DataTypes: Sequelize.DataTypes): Sequelize.Model<ProductInstance, ProductAttributes> => {
  const attributes: SequelizeAttributes<ProductAttributes> = {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    description: {
      type: DataTypes.STRING,
      validate: {
        len: [0, 200],
      },
    },
    productType: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [2, 200],
      },
    },
    imageSrc1: {
      type: DataTypes.STRING,
      validate: {
        isUrl: true,
      },
    },
    imageSrc2: {
      type: DataTypes.STRING,
      validate: {
        isUrl: true,
      },
    },
    imageSrc3: {
      type: DataTypes.STRING,
      validate: {
        isUrl: true,
      },
    },
    imageSrc4: {
      type: DataTypes.STRING,
      validate: {
        isUrl: true,
      },
    },
    discount: {
      type: DataTypes.INTEGER,
    },
    price: {
      type: DataTypes.DOUBLE,
      allowNull: false,
    },
  };

  const Product = sequelize.define<ProductInstance, ProductAttributes>('Product', attributes);

  return Product;
}
