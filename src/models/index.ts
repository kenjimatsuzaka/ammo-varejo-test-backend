import Sequelize from 'sequelize';
import { DbInterface } from'typings/DbInterface';
import { ProductFactory } from './product';

export const createModels = (sequelizeConfig: any): DbInterface => {
  const sequelize = new Sequelize(sequelizeConfig);

  const db: DbInterface = {
    sequelize,
    Sequelize,
    Product: ProductFactory(sequelize, Sequelize)
  };

  return db;
};
