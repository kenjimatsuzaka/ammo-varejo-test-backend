import * as Sequelize from "sequelize";
import { ProductAttributes, ProductInstance } from "../../models/product";

export interface DbInterface {
  sequelize: Sequelize.Sequelize;
  Sequelize: Sequelize.SequelizeStatic;
  Product: Sequelize.Model<ProductInstance, ProductAttributes>;
}
