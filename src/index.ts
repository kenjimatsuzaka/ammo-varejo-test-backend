import app from './app';

let port = process.env.PORT || 8080;
if (process.env.NODE_ENV === 'test') port = 8081;

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
