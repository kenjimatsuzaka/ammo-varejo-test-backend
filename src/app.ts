import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import { ProductRouter } from './routes/productRouter';

class App {
  public app: express.Application;
  public productRouter: ProductRouter = new ProductRouter();

  constructor() {
    this.app = express();
    this.config();
    this.productRouter.routes(this.app);
    this.app.use((err, req, res, next) => {
      res.status(400).send(err.message);
    });
  }

  private config(): void {
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json());
    this.app.use(cors());
  }
}

export default new App().app;
