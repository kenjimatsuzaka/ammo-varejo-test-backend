import { createModels } from '../models';

const sequelizeConfig = require('../../config/config.json')[process.env.NODE_ENV];
const db = createModels(sequelizeConfig);

export class ProductController {

  public getProducts(filter) {
    let query = {};

    if (filter.q) {
      query = {
        [db.Sequelize.Op.or]: [
          { productType: { $like: `%${filter.q}%` } },
          { name: { $like: `%${filter.q}%` } },
          { description: { $like: `%${filter.q}%` } },
        ],
      };

      return db.Product.findAndCountAll({
        where: query,
      });
    }

    return db.Product.findAndCountAll();
  }
}
