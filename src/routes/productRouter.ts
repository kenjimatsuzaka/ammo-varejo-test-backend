import { Request, Response } from "express";
import { ProductController } from '../controllers/productController';

export class ProductRouter {
  public productController: ProductController = new ProductController();

  public routes(app): void {
      app.route('/products')
      .get(async(req: Request, res: Response) => {
          try {
            const products = await this.productController.getProducts(req.query);
            return res.status(200).send(products);
          } catch (error) {
            return res.status(400).send(error.message);
          }
      })
  }
}
