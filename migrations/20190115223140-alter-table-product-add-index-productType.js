module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addIndex('Products', {
      fields: ['productType'],
      name: 'ProductType_idx',
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeIndex('Products', 'ProductType_idx');
  }
};
