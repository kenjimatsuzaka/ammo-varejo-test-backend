
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('Products',
        {
          id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
          },
          createdAt: { type: Sequelize.DATE },
          updatedAt: { type: Sequelize.DATE },
          deletedAt: { type: Sequelize.DATE },
          name: {
            type: Sequelize.STRING,
            allowNull: false,
          },
          description: {
            type: Sequelize.STRING,
          },
          productType: {
            type: Sequelize.STRING,
            allowNull: false,
          },
          imageSrc1: {
            type: Sequelize.STRING,
          },
          imageSrc2: {
            type: Sequelize.STRING,
          },
          imageSrc3: {
            type: Sequelize.STRING,
          },
          imageSrc4: {
            type: Sequelize.STRING,
          },
          discount: {
            type: Sequelize.INTEGER,
          },
          price: {
            type: Sequelize.DOUBLE,
            allowNull: false,
          },
        });
  },

  down: (queryInterface, Sequelize) => {},
};
